package routers

import (
	"github.com/gin-gonic/gin"
	"telunjuk-rest/controllers"
)

func InitRouter() *gin.Engine {
	r := gin.New()
	r.Use(gin.Recovery())

	r.POST("/point", controllers.PointController)
	r.POST("/user", controllers.ListuserController)
	return r
}
