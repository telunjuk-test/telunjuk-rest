package main

import (
	"fmt"
	"log"
	"runtime"
	"strconv"
	"telunjuk-rest/routers"
	"telunjuk-rest/utils"
	//_"telunjuk-rest/kafka/producer"
)

var(
	port string
)

func main()  {


	maxProc, _ := strconv.Atoi(utils.GetEnv("MAXPROCS", "1"))
	port = utils.GetEnv("PORT", "8001")
	runtime.GOMAXPROCS(maxProc)

	routersInit := routers.InitRouter()
	endPoint := fmt.Sprintf(":%d", port)

	log.Println("[info] start http server listening %s", endPoint)

	//server.ListenAndServe()

	routersInit.Run(":" + port)

}
