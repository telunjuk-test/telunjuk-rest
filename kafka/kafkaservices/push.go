package kafkaservices

import (
	"errors"
	"github.com/Shopify/sarama"
	"github.com/astaxie/beego/logs"
	"telunjuk-rest/kafka/producer"
)

type ServicePush struct {
	Topic       string
	Brokerlist  string
	ConfigKafka *sarama.Config
}

// The buypulse service ...
func (s *ServicePush) Push(bdata []byte) error {

	logs.Info("Produce to Kafka")

	kafkaProducer := producer.GetConectionKafka()
	if kafkaProducer.ErrRes != nil {
		logs.Error("Err Client REQ :", kafkaProducer.ErrRes)
		return kafkaProducer.ErrRes
	}
	kafkaProducer.Topic = s.Topic //fmt.Sprintf("%s-req", s.Topic)
	kafkaProducer.Value = sarama.ByteEncoder(bdata)
	kafkaProducer.Process()
	if kafkaProducer.Done == false {
		return errors.New("Sending to Kafka Failed")
	}

	return nil
}
