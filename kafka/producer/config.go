package producer

import (
	"log"
	"os"
	"strings"
	"telunjuk-rest/utils"
	"time"

	"github.com/Shopify/sarama"
)

var (
	kafkaBrokerUrl string
	kafkaTopic     string
	kafkaClient    string

	maxmsgbyte     int
	producerconn sarama.SyncProducer
	ErrKafka 	   error
	kafkaTimeout   string
	kafkaDialTimeout string
	kafkaReadTimeout string
	kafkaWriteTimeout string
)



func init() {
	kafkaBrokerUrl = utils.GetEnv("KAFKA_BROKERS", "localhost:9092,localhost:9093,localhost:9094")
	kafkaClient = "client1"
	maxmsgbyte = 50000000
	kafkaTimeout = "10s"
	kafkaDialTimeout = "10s"
	kafkaReadTimeout = "10s"
	kafkaWriteTimeout = "10s"

	Connection()
}

func GetProducerConn()  sarama.SyncProducer  {
	return producerconn
}

//Get Connection Kafka
func GetConectionKafka() KafkaProducer {

	return KafkaProducer{
		Connection: producerconn,
		ErrRes:     ErrKafka,
	}
}

func Connection()  {

	producerconn, ErrKafka = sarama.NewSyncProducer(strings.Split(kafkaBrokerUrl, ","), GetConfigKafka())
}


func GetKafkaBroker() string {
	return kafkaBrokerUrl
}

func GetTopic() string {
	return kafkaTopic
}

func GetConfigKafka() *sarama.Config {
	prodTo , _ := time.ParseDuration(kafkaTimeout)
	dialTo , _ := time.ParseDuration(kafkaDialTimeout)
	readTo , _ := time.ParseDuration(kafkaReadTimeout)
	writeTo , _ := time.ParseDuration(kafkaWriteTimeout)


	sarama.Logger = log.New(os.Stdout, "[sarama] ", log.LstdFlags)
	configKafka := sarama.NewConfig()
	configKafka.Producer.Return.Successes = true
	configKafka.Producer.Partitioner = sarama.NewRandomPartitioner
	configKafka.ClientID = kafkaClient
	configKafka.Producer.MaxMessageBytes = maxmsgbyte
	configKafka.Producer.Timeout = prodTo
	configKafka.Net.DialTimeout = dialTo
	configKafka.Net.ReadTimeout = readTo
	configKafka.Net.WriteTimeout = writeTo

	return configKafka
}
