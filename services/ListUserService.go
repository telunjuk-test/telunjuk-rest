package services

import (
	"telunjuk-rest/db"
	"telunjuk-rest/models/dbmodels"
)

type ListUserService struct {
	ListUserRepo *db.ListUserRepo
}

func InitListUserService() *ListUserService {
	return &ListUserService{
		ListUserRepo: db.InitListUserRepo(),
	}
}

func (s *ListUserService) FindByPhoneNumber(phoneNumber string) (dbmodels.ListUser, error) {

	listUser, err :=s.ListUserRepo.FindByPhoneNumber(phoneNumber)

	return listUser, err
}
