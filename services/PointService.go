package services

import (
	"encoding/json"
	"telunjuk-rest/kafka/kafkaservices"
	"telunjuk-rest/models"
	"telunjuk-rest/utils"
)

type PointService struct {
}

func InitPointService() *PointService {
	return &PointService{}
}

func (s *PointService) SendToKafka(req models.ReqPoint) error {

	topic := utils.GetEnv("KAFKA_TOPIC_SEND_POINT", "test001")
	svc := kafkaservices.ServicePush{
		Topic:       topic,
	}

	formInBytes, _ := json.Marshal(req)
	errKafka :=  svc.Push(formInBytes)

	return errKafka

}