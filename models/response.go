package models

type Response struct {
	Rc string `json:"rc"`
	Msg string `json:"msg"`
	Data interface{} `json:"data"`
} 
