package models

import "time"

type ReqPoint struct {
	PhoneNumber string `json:"phoneNumber"`
	Value 	int64 `json:"value"`
	Key 	string `json:"key"`
	TransactionTime time.Time `json:"transactionTime"`
} 
