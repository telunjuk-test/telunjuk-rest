package db

import (
	"log"
	"telunjuk-worker/models/dbmodels"
	"testing"
)

func TestPointTransactionRepo_Save(t *testing.T) {
	req := dbmodels.PointTransaction{
		 PhoneNumber: "081394320721",
		 Value: 15,
		 Key: "11223344",
	}
	err := InitPointTransactionRepo().Save(&req)
	log.Println(err)
}