package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"telunjuk-rest/models"
	"telunjuk-rest/services"
)

//ListuserController
func ListuserController(ctx *gin.Context) {

	var req models.ReqPoint
	var res models.Response

	if err := ctx.ShouldBindJSON(&req); err != nil {
		fmt.Println("Failed to bind request:", err)
		res.Rc = "03"
		res.Msg = "Error Unmarshal"
		ctx.JSON(http.StatusOK, res)
		return
	}

	reqByte,_ := json.Marshal(req)
	log.Println("request --> ", string(reqByte))

	listUser, err := services.InitListUserService().FindByPhoneNumber(req.PhoneNumber)
	if err != nil {
		res.Rc = "01"
		res.Msg = err.Error()
		ctx.JSON(http.StatusOK, res)
		return
	}

	res.Rc = "00"
	res.Msg = "Success"
	res.Data = listUser

	ctx.JSON(http.StatusOK, res)

}
