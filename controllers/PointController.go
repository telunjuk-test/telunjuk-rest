package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"log"
	"net/http"
	"telunjuk-rest/models"
	"telunjuk-rest/services"
	"time"
)

//PointController
func PointController(ctx *gin.Context) {

	var req models.ReqPoint
	var res models.Response

	if err := ctx.ShouldBindJSON(&req); err != nil {
		fmt.Println("Failed to bind request:", err)
		res.Rc = "03"
		res.Msg = "Error Unmarshal"
		ctx.JSON(http.StatusOK, res)
		return
	}

	req.Key = uuid.New().String()
	req.TransactionTime = time.Now()
	reqByte,_ := json.Marshal(req)
	log.Println("request --> ", string(reqByte))

	err := services.InitPointService().SendToKafka(req)
	if err != nil {
		res.Rc = "01"
		res.Msg = err.Error()
		ctx.JSON(http.StatusOK, res)
		return
	}

	res.Rc = "00"
	res.Msg = "Success"

	ctx.JSON(http.StatusOK, res)

}
